# This is just an example to get you started. A typical library package
# exports the main API in this file. Note that you cannot rename this file
# but you can remove it if you wish.

import sugar, httpclient


type
  Service* {.pure.} = enum
    EC2 = "ec2",
    S3 = "s3",
    IAM = "iam"

proc add*(x, y: int): int =
  return x + y
