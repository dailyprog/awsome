#!/bin/sh

apt-get install -yqq make
wget https://nim-lang.org/download/nim-0.19.4.tar.xz
tar xf nim-0.19.4.tar.xz
cd nim-0.19.4
sh build.sh
bin/nim c koch
./koch tools
ln -s `pwd`/bin/nim /bin/nim
ln -s `pwd`/bin/nimble /bin/nimble
cd ..
